class TextButton {
    constructor(x,y,width,height,radius) {
        this.width = width;
        this.gfx = new PIXI.Graphics();
        this.gfx.beginFill(0x5599ff);
        this.gfx = this.gfx.drawRoundedRect(0,0,width,height,radius);
        this.gfx.x = x;
        this.gfx.y = y;
        this.gfx.endFill();
        this.text = undefined;
    }

    addText(text, fontSize) {
        const textstyle = new PIXI.TextStyle({
            fontFamily: 'Sans Serif',
            fontSize: fontSize,
            fontWeight: 'bold',
            wordWrap: true,
            wordWrapWidth: 4*this.width/5
        });
        if (this.text !== undefined)
        {
            app.stage.removeChild(this.text);
            delete this.text;
        }
        this.text = new PIXI.Text(text, textstyle);
        this.text.anchor.set(0.5);
        this.text.x = this.gfx.x + this.gfx.width/2;
        this.text.y = this.gfx.y + this.gfx.height/2;
        this.text.visible = false;
        return this;
    }

    setVisible(bool) {
        this.gfx.visible = bool;
        if (this.text !== undefined)
        {
            this.text.visible = bool;
        }
        return this;
    }

    enable() {
        this.gfx.interactive = true;
        this.gfx.buttonMode = true;
        app.stage.addChild(this.gfx);
        app.stage.addChild(this.text);
        return this;
    }

    disable() {
        this.gfx.interactive = false;
        this.gfx.buttonMode = false;
        app.stage.removeChild(this.gfx);
        app.stage.removeChild(this.text);
        return this;
    }

    onTapCallback(f) {
        this.gfx.on('click', f);
        this.gfx.on('tap', f);
        return this;
    }    
}