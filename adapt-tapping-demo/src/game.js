  let type = "WebGL"
  if(!PIXI.utils.isWebGLSupported()){
    type = "canvas"
  }
  PIXI.utils.sayHello(type)

  // Create Pixi Application
  let app = new PIXI.Application({
      width: 1000, 
      height: 600,
      antialias: true,
      transparent: false,
      resolution: 1
  });

  app.renderer.backgroundColor = 0xFFFFFF;
  app.width = 1000;
  app.height = 600;
  document.body.appendChild(app.view);

  let gamestate = new Object();
  let keyboard = new Keyboard();
  let mouse = new Mouse();

  function setup() {
      let url_params = new URLSearchParams(window.location.search);
      let rule = url_params.get("rule");
      gamestate.title_screen = new TitleScreen();
      gamestate.task_screen = new TaskScreen(1.0);
      gamestate.task_screen.setUpdateRule(rule);
      gamestate.thankyou_screen = new ThankYouScreen();
      gamestate.plot_screen = new PlotScreen();
      gamestate.state = "init"
      app.ticker.add(delta => gameLoop(delta));
  }


  setup();

  function gameLoop(delta)
  {
    if (gamestate.state === "init")
    {
      gamestate.state = "title";
      gamestate.title_screen.activate();
    }

    else if (gamestate.state === "title")
    {
      if (gamestate.title_screen.active)
      {
        gamestate.title_screen.update(delta);
      } 
      else 
      {
        gamestate.state = "task";
        gamestate.task_screen.activate();
      }
    }

    else if (gamestate.state === "task") 
    {
      if (gamestate.task_screen.active) 
      {
        gamestate.task_screen.update(delta)
      } 
      else
      {
        gamestate.state = "game over"
        gamestate.thankyou_screen.activate();
      }
    }

    else if (gamestate.state === "game over")
    {
        if (gamestate.thankyou_screen.active)
        {
          gamestate.thankyou_screen.update(delta);
        }
        else
        {
          if (gamestate.thankyou_screen.plot)
          {
            gamestate.state = "plot";
            let task_data = gamestate.task_screen.getTaskData();
            gamestate.plot_screen.activate(task_data);
          }
          else
          {
            gamestate.state = "title";
            gamestate.title_screen.activate();
          }
        }
    }

    else if (gamestate.state === "plot")
    {
      if (gamestate.plot_screen.active)
      {

      }
    }
  }
    