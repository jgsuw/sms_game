
class TaskScreen {
    constructor(starting_bounce) {
        this.name = 'task';
        this.active = false;
        this.duration = 60e3;
        this.update_rule = this.updateRule1.bind(this);

        this.starting_bounce = starting_bounce;
        this.bounce_period = this.starting_bounce;
        this.ground = 100;
        this.gravity = 8*(app.height/2-this.ground)/this.bounce_period**2;

        // Set up the ball
        let ball = new Object();
        this.ball = ball;
        ball.gfx = new PIXI.Graphics();
        ball.radius = 50;
        ball.gfx.beginFill(0xFF0000);
        ball.gfx.drawCircle(0,0,ball.radius);
        ball.gfx.endFill();
        ball.gfx.visible = false;
        ball.setPos = (x,y) => {
            let xnew, ynew;
            [xnew, ynew] = this.coordTransform(x,y);
            ball.gfx.x = xnew;
            ball.gfx.y = ynew;
        }
        ball.getPos = () => {
            let x, y;
            [x,y] = this.coordTransform(ball.gfx.x, ball.gfx.y);
            return [x,y];
        }
        ball.v = 0;
        ball.setPos(app.width/2, .5*this.gravity);

        // Set up the bar which represents the ground
        let bar = new PIXI.Graphics();
        this.bar = bar;
        bar.beginFill(0xACACAC);
        bar.drawRect(0,0,app.width,this.ground);
        bar.endFill();
        bar.x = 0;
        bar.y = app.height - this.ground;
        bar.visible = false;

        // Load sound for playing bounce
        PIXI.sound.add('bounce', '../resources/bounce1.mp3');

        // Register keyboard and touch event listeners
        keyboard.addKeyEventListener(" ", this.spacebarCallback.bind(this));
	let body = document.getElementById("body");
        body.addEventListener("touchstart", this.spacebarCallback.bind(this),false);

        // variables for recording tap / bounce times
        this.tap_times = undefined;
        this.last_tap = undefined;
        this.bounce_times = undefined;
        this.next_bounce = undefined;
        this.last_bounce = undefined;
        this.asynchronies = undefined;
        this.period_estimate = this.bounce_period;
    }

    activate() {
        this.active = true;
        this.ball.gfx.visible = true;
        app.stage.addChild(this.ball.gfx);
        this.bar.visible = true;
        app.stage.addChild(this.bar);
        this.start_time = new Date().getTime();
        this.bounce_times = [];
        this.tap_times = [];
        this.asynchronies = [];
        this.next_bounce = this.bounce_period;
    }

    deactivate() {
        this.active = false;
        this.ball.gfx.visible = false;
        app.stage.removeChild(this.ball.gfx);
        this.bar.visible = false;
        app.stage.removeChild(this.bar);
    }

    coordTransform(x,y) {
        return [x,app.height - y - this.ground];
    }

    spacebarCallback(event) {
        if (!this.active) {return;}
        if (event.type === "keydown" || event.type ==="touchstart")
        {
            if (!event.repeat)
            {
                this.last_tap = new Date().getTime()-this.start_time;
                this.tap_times.push(this.last_tap);
            }
        }
    }

    getTaskData()
    {
        let data = {
            asyns: this.asynchronies,
            timestamps: this.tap_times
        };
        return data;
    }

    updateData()
    {   
        let this_bounce = new Date().getTime()-this.start_time;
        this.bounce_times.push(this_bounce);
        if (this.last_bounce !== undefined)
        {
            if (this.tap_times.length > 0)
            {
                let last_tap = this.tap_times[this.tap_times.length-1];
                let asyn = 0;
                if (last_tap - this.last_bounce < this_bounce - last_tap)
                {   // positive asynchrony
                    asyn = last_tap - this.last_bounce;
                    this.asynchronies.push(asyn);
                }
                else
                {
                    // negative asynchrony
                    asyn = last_tap - this_bounce;
                    this.asynchronies.push(asyn);
                }
            }
        }
        // console.log(this.asynchronies[this.asynchronies.length-1]);
        this.last_bounce = this_bounce;
    }

    updateRule1()
    {   // phase and period correction
        const alpha = .65;
        const beta = .25;
        if (this.asynchronies.length > 0)
        {
            let asyn = this.asynchronies[this.asynchronies.length-1];
            let phase_correction = (alpha+beta)*asyn;
            let dt = this.bounce_period + 1.*phase_correction/1000;
            this.next_bounce = this.last_bounce + dt;
            let period_correction = beta*asyn;
            this.bounce_period += 1*period_correction/1000;
            return dt;
        }
        else
        {
            return 0;
        }
    }

    updateRule2()
    {   // phase correction
        const alpha = 0.75;
        if (this.asynchronies.length > 0)
        {
            let asyn = this.asynchronies[this.asynchronies.length-1];
            let correction = alpha*asyn;
            let dt = this.bounce_period + 1.*correction/1000;
            this.next_bounce = this.last_bounce + dt;
            return dt;
        }
        else
        {
            return 0;
        }
    }

    updateRule3()
    {
        // joey style
        const alpha = 0.7;
        const beta = 0.5;
        let dt = 0;
        if (this.tap_times.length > 2)
        {
            let asyn = this.asynchronies[this.asynchronies.length-1];
            // Phase correction
            let correction = alpha * asyn;
            dt = this.period_estimate + 1*correction/1000;
            // Period correction
            let len = this.tap_times.length;
            let iti = this.tap_times[len-1]-this.tap_times[len-2];
            // console.log(iti_avg);
            this.period_estimate = (1-beta)*this.period_estimate + beta * 1*iti/1000;
            console.log(this.period_estimate);
        }
        else
        {
            dt = this.period_estimate;
        }
        return dt;
    }

    setUpdateRule(rule) {
        if (rule==="1")
        {
            this.update_rule = this.updateRule1.bind(this);
        }
        else if (rule==="2")
        {
            this.update_rule = this.updateRule2.bind(this);
        }
        else if (rule==="3")
        {
            this.update_rule = this.updateRule3.bind(this);
        }
    }


    update(delta) {
        let dt = delta/60;
        let ball = this.ball;

        // Set new ball position
        let x, y;
        [x,y] = this.ball.getPos();
        ball.setPos(x,y+ball.v*dt);
        [x,y] = ball.getPos();

        // Check for collision
        if (y - ball.radius < 0) {
            this.updateData();
            let dt = this.updateRule3();
            const min_dt = 0.1;
            const max_dt = 2;
            dt = Math.min(max_dt,Math.max(0,dt));
            console.log(dt)
            ball.setPos(x, ball.radius);
            if (dt < min_dt)
            {
                this.deactivate();
                return;
            }
            else
            {
                PIXI.sound.play('bounce');
                ball.v = (this.gravity*dt/2);
            }
        }
        // Else update ballistic trajectory
        else {
            ball.v += -this.gravity*dt;
        }

        // terminate after alloted time has passed
        let d = new Date();
        if (d.getTime()-this.start_time > this.duration) {
            this.deactivate();
        }
    }
}
