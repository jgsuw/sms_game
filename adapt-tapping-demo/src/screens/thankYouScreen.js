class ThankYouScreen {
    constructor() {
        this.name = 'title';
        this.active = false;

        this.nextButton = new TextButton(1.*app.width/3-150/3, 2.*app.height/3-100/2, 150, 100, 25);
        this.nextButton.onTapCallback(this.nextButtonCallback.bind(this));
        this.nextButton.addText("play again?", 36);

        this.plotButton = new TextButton(2.*app.width/3-150/3, 2.*app.height/3-100/2, 150, 100, 25);
        this.plotButton.onTapCallback(this.plotButtonCallback.bind(this));
        this.plotButton.addText("plot result?", 36);

        this.plot = false;
        this.next = false;

        // Text messages for rendering
        const textstyle = new PIXI.TextStyle({
            fontFamily: 'Sans Serif',
            fontSize: 36,
            fontWeight: 'bold',
            wordWrap: true,
            wordWrapWidth: 2*app.width/3
        });

        this.intro_message = new PIXI.Text('Thanks for playing!', textstyle);
        this.intro_message.anchor.set(0.5);
        this.intro_message.x = app.width/2;
        this.intro_message.y = app.height/3;
        this.intro_message.visible = false;
    }   

    nextButtonCallback(event) {
        this.next = true;
        this.plot = false;
        this.deactivate();
    }

    plotButtonCallback(event) {
        this.next = false;
        this.plot = true;
        this.deactivate();
    }

    activate() {
        this.active = true;
        this.plot = false;
        this.next = false;
        this.nextButton.enable().setVisible(true);
        this.plotButton.enable().setVisible(true);
        this.intro_message.visible = true;
        app.stage.addChild(this.intro_message);
    }

    deactivate() {
        this.active = false;
        this.nextButton.disable().setVisible(false);
        this.plotButton.disable().setVisible(false);
        this.intro_message.visible = false;
        app.stage.removeChild(this.intro_message);
    }

    update(delta) {
    }
}