class TitleScreen {
    constructor() {
        this.name = 'title';
        this.active = false;
        this.nextButton = new TextButton(app.width/2-150/2,2*app.height/3-100/2,150,100,25);
        this.nextButton.onTapCallback(this.onClick.bind(this));
        this.nextButton.addText("continue", 36);

        // Text messages for rendering
        const textstyle = new PIXI.TextStyle({
            fontFamily: 'Sans Serif',
            fontSize: 36,
            fontWeight: 'bold',
            wordWrap: true,
            wordWrapWidth: 2*app.width/3
        });

        this.intro_message = new PIXI.Text('Try to synchronize with the bouncing ball by pressing the space-bar when it bounces.', textstyle);
        this.intro_message.anchor.set(0.5);
        this.intro_message.x = app.width/2;
        this.intro_message.y = app.height/3;
        this.intro_message.visible = false;
    }   

    onClick(event) {
        this.deactivate();
    }

    activate() {
        this.active = true;
        this.nextButton.enable().setVisible(true);
        app.stage.addChild(this.intro_message);
        this.intro_message.visible = true;
    }

    deactivate() {
        this.active = false;
        this.nextButton.disable()
        this.intro_message.visible = false;
        app.stage.removeChild(this.intro_message);
    }

    update(delta) {
        // this.nextButton.setVisible(true);
    }
}