class PlotScreen {

    constructor() {

        this.active = false;

    }

    activate(data) {
        this.active = true;
        var trace = {
            x: data.timestamps,
            y: data.asyns,
            mode: 'markers',
            marker: {
                color: 'rgb(219,64,82)',
                size: 12
            }
        };

        var layout = {
            title: 'Asynchrony vs Time'
        };

        let div = document.getElementById('body');
        Plotly.newPlot(div, [trace], layout);             
    }

    deactivate() {
        this.active = false;
        let div = document.getElementById('body');
        Plotly.purge(div);
   }

    update(delta) {

    }
}