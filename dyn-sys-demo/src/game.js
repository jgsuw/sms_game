    let type = "WebGL"
    if(!PIXI.utils.isWebGLSupported()){
        type = "canvas"
    }
    PIXI.utils.sayHello(type)

    // Create Pixi Application
    let app = new PIXI.Application({
        width: 450, 
        height: 800,
        antialias: true,
        transparent: false,
        resolution: 1
    });

    app.renderer.backgroundColor = 0xFFFFFF;
    app.width = 450;
    app.height = 800;
    document.body.appendChild(app.view);

    let gamestate = new Object();
    let keyboard = new Keyboard();

    function setup() {
        gamestate.state = "start";
        // Start button creation
        gamestate.start_button = new TextButton(150,75,25);
        gamestate.start_button.setLocation((app.width-150)/2, (app.height-75)/2);
        gamestate.start_button.addText('Start',48);
        gamestate.start_button.onTapCallback((event) => 
            {
                gamestate.state = "session";
                let url = new URL(window.location);
                let a = -1;
                if (url.searchParams.has("a"))
                {
                    a = parseFloat(url.searchParams.get("a"));
                }
                let x = 1;
                if (url.searchParams.has("x"))
                {
                    x = parseFloat(url.searchParams.get("x"))
                }
                let b = x*(1-a);
                gamestate.session = new Session(a,b,1.);
                gamestate.session.activate();
                gamestate.start_button.disable();
                gamestate.start_button.setVisible(false);
            }
        );
        gamestate.start_button.enable();
        gamestate.start_button.setVisible(true);

        app.ticker.add(delta => gameLoop(delta));
    }


    setup();

    function gameLoop(delta)
    {
        if (gamestate.state === "session")
        {
            if (gamestate.session.active)
            {
                gamestate.session.gameloop(delta);
            }
        }
    }
