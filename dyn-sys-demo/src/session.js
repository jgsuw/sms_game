
class Session{
    // TODO: change constructor signature to accept new arguments (control map slope and y-intercept)
    constructor(a,b,starting_IOI) {
        this.a = a;
        this.b = b;
        this.active = false;
        this.duration = 60e3;

        this.starting_IOI = starting_IOI;
        this.ground = 100;
        this.gravity = 8*(app.height/2-this.ground)/this.starting_IOI**2;

        // Set up the ball
        let ball = new Object();
        this.ball = ball;
        ball.gfx = new PIXI.Graphics();
        ball.radius = 50;
        ball.gfx.beginFill(0xFF0000);
        ball.gfx.drawCircle(0,0,ball.radius);
        ball.gfx.endFill();
        ball.gfx.visible = false;
        ball.setPos = (x,y) => {
            let xnew, ynew;
            [xnew, ynew] = this.coordTransform(x,y);
            ball.gfx.x = xnew;
            ball.gfx.y = ynew;
        }
        ball.getPos = () => {
            let x, y;
            [x,y] = this.coordTransform(ball.gfx.x, ball.gfx.y);
            return [x,y];
        }
        ball.v = 0;
        ball.setPos(app.width/2, app.height/2-this.ground            );

        // Set up the bar which represents the ground
        let bar = new PIXI.Graphics();
        this.bar = bar;
        bar.beginFill(0xACACAC);
        bar.drawRect(0,0,app.width,this.ground);
        bar.endFill();
        bar.x = 0;
        bar.y = app.height - this.ground;
        bar.visible = false;

        // Load sound for playing bounce
        PIXI.sound.add('bounce', '../resources/bounce1.mp3');

        // Register keyboard and touch event listeners
        window.addEventListener("keydown", this.tapCallback.bind(this));
	    let body = document.getElementById("body");
        body.addEventListener("touchstart", this.tapCallback.bind(this), false);

        // variables for recording tap / bounce times
        this.tap_times = undefined;
        this.last_tap = undefined;
        this.bounce_times = undefined;
        this.next_bounce = undefined;
        this.last_bounce = undefined;
        this.asynchronies = undefined;
        this.ITI = undefined;                       // inter-onset-interval
        this.IOI = undefined;                       // inter-tap-interval
        this.period_estimate = undefined;
    }

    activate() {
        this.active = true;
        this.ball.gfx.visible = true;
        app.stage.addChild(this.ball.gfx);
        this.bar.visible = true;
        app.stage.addChild(this.bar);
        this.start_time = new Date().getTime();
        this.bounce_times = [];
        this.tap_times = [];
        this.asynchronies = [];
        this.next_bounce = this.bounce_period;
    }

    deactivate() {
        this.active = false;
        this.ball.gfx.visible = false;
        app.stage.removeChild(this.ball.gfx);
        this.bar.visible = false;
        app.stage.removeChild(this.bar);
    }

    coordTransform(x,y) {
        return [x,app.height - y - this.ground];
    }

    tapCallback(event) {
        if (!this.active) {return;}
        if (event.type === "keydown" || event.type ==="touchstart")
        {
            if (!event.repeat)
            {   
                let t = new Date().getTime()-this.start_time;
                this.tap_times.push(t);
                if (this.last_tap === undefined)
                {
                    this.period_estimate = this.starting_IOI;
                }
                else
                {
                    this.period_estimate = (.25*this.period_estimate + .75*(t-this.last_tap))/1000.;
                }
                this.last_tap = t;
            }
        }
    }

    getTaskData()
    {
        let data = {
            asyns: this.asynchronies,
            timestamps: this.tap_times
        };
        return data;
    }

    updateData()
    {   
        let this_bounce = new Date().getTime()-this.start_time;
        this.bounce_times.push(this_bounce);
        if (this.last_bounce !== undefined)
        {
            if (this.tap_times.length > 0)
            {
                let last_tap = this.tap_times[this.tap_times.length-1];
                let asyn = 0;
                if (last_tap - this.last_bounce < this_bounce - last_tap)
                {   // positive asynchrony
                    asyn = last_tap - this.last_bounce;
                    this.asynchronies.push(asyn);
                }
                else
                {
                    // negative asynchrony
                    asyn = last_tap - this_bounce;
                    this.asynchronies.push(asyn);
                }
            }
        }
        this.last_bounce = this_bounce;
    }

    IOIUpdate()
    {
        let dt = this.starting_IOI
        if (this.period_estimate !== undefined)
        {
            dt = this.a * this.period_estimate + this.b;
        }
       return dt;
    }

    gameloop(delta) {
        let dt = delta/60;
        let ball = this.ball;

        // Set new ball position
        let x, y;
        [x,y] = this.ball.getPos();
        ball.setPos(x,y+ball.v*dt);
        [x,y] = ball.getPos();

        // Check for collision
        if (y - ball.radius < 0) {
            this.updateData();
            let dt = this.IOIUpdate();
            const min_dt = 0.1;
            const max_dt = 4;
            dt = Math.min(max_dt,Math.max(min_dt,dt));
            console.log(dt)
            ball.setPos(x, ball.radius);
            if (dt < min_dt)
            {
                this.deactivate();
                return;
            }
            else
            {
                PIXI.sound.play('bounce');
                ball.v = (this.gravity*dt/2);
            }
        }
        // Else update ballistic trajectory
        else {
            ball.v += -this.gravity*dt;
        }

        // terminate after alloted time has passed
        let d = new Date();
        if (d.getTime()-this.start_time > this.duration) {
            this.deactivate();
        }
    }
}
