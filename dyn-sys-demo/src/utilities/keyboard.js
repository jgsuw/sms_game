class Keyboard {
    constructor() {
        window.addEventListener("keydown", this.keyHandler.bind(this), false);
        window.addEventListener("keyup", this.keyHandler.bind(this), false);
        this.listeners = [];
        // this.addKeyEventListener(" ", (event) => {console.log("spacebar " + event);});
    }
    
    keyHandler(event) {
        this.listeners.forEach( (listener) => {
            if (listener.key === event.key)
            {
                listener.callback(event);
            }
        });
    }

    addKeyEventListener(key, callback) {
        let listener = new Object();
        listener.key = key;
        listener.callback = callback;
        this.listeners.push(listener);
    }
}

class Mouse {
    constructor() {
        window.addEventListener("onmousedown", this.onClickHandler, false);
    }
    onClickHandler(event) {
        console.log("Mouse click!");
    }
}